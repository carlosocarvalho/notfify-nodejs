var moment = require('moment-timezone');

function ClockWebsocket() {}

var clockUpdate = null;

ClockWebsocket.prototype.start = function(io) {
    clockUpdate = setInterval(function() {
        io.emit('update:clock', JSON.stringify({ 'time': moment().tz("America/Sao_Paulo").format() }));
    }, 900);
}
ClockWebsocket.prototype.stop = function() {
    clearInterval(clockUpdate);
}
module.exports = new ClockWebsocket();