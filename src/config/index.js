var dotenv = require('dotenv').config().parsed;

/**
 *  Config Dotenv file
 * 
 * @example file .env
 * `
 *   MYSQL_SERVER=mysql
 *   MYSQL_PORT=2206
 *   MYSQL_USER=root
 *   MYSQL_DATABSE=demo
 * ` 
 * 
 */


function ConfigSetup() {
    this._properties = {};
    this.initialize();
}

/**
 * initialize all config dont env file
 * @return void 
 */
ConfigSetup.prototype.initialize = function() {
    for (var i in dotenv) {
        var config = buildKeyValue(i);
        if (!this._properties[config.key]) this._properties[config.key] = {};
        this._properties[config.key][config.value] = dotenv[i];
    }
};
/**
 * get propertie config by key
 * @param string key 
 * @param string value
 * @return object|sttring
 */
ConfigSetup.prototype.get = function(key, value) {
    var v = value || false;
    if (!this._properties.hasOwnProperty(key)) return false;
    if (!v)
        return this._properties[key];
    return this._properties[key][v];
};
/**
 * get all properties
 * @return object
 */
ConfigSetup.prototype.all = function() {
    return this._properties;
};

/**
 * build key value dotenv file especifications
 * @param string str 
 * @returns object
 */
function buildKeyValue(str) {
    var toLower = (str + '').toLowerCase();
    var spl = toLower.split('_');
    return {
        key: spl[0],
        value: spl[1]
    };
}





module.exports = new ConfigSetup();