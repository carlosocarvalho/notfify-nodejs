var config = require('./src/config');
var redis = require('redis');
var io = require('socket.io')(config.get('ws', 'port'));
var clockwesocket = require('./src/clockwebsocket');
var redisSetup = config.get('redis');
io.set('transports', ['polling', 'websocket']);

var pub = redis.createClient(redisSetup.port, redisSetup.server);
var Users = 0;

//start 
//clockwesocket.start(io);

var allChannels = [
    'bid:created',
	'notify',
	'bid:off',
	'lote:update',
	'lote:destroy',
	'lote:create',
	'auction:status',
	'auction:close',
	'update:panel'
];

io.on('connection', function(socket) {
    ++Users;
    var sub = connect();  
	    subcriber(sub);
	sub.on('message', function(channel, message) {
        console.log(channel + '>>' + message);
        socket.emit(channel, message);
    });
    socket.on('disconnect', function(channel) {
        --Users;
		sub.unsubscribe();
		sub.quit();
		console.log('user disconnect');
        
    });
    console.log(Users);
});


function connect(){
	 return redis.createClient(redisSetup.port, redisSetup.server);
}


function subcriber(sub){
	for(var i in allChannels){
		var ch = allChannels[i];
		sub.subscribe(ch);
	}
}


console.log('Server started...' + config.get('ws', 'port'));