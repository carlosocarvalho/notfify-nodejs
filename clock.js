let config = require('./src/config');
var moment = require('moment-timezone');
var io = require('socket.io')(config.get('ws', 'port'));
var clockUpdate = setInterval(function() {
    io.emit('update:clock', JSON.stringify({ 'time': moment().tz("America/Sao_Paulo").format() }));
}, 900);